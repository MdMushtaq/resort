<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'blueworld' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(0Jzbl/yRDj~z| wEm]8%#!M`$@}W7]S M.+1c5{3.4_zKDq*zfMG1bBvg!!mq@u' );
define( 'SECURE_AUTH_KEY',  '_)=zoWnPo9g1e}}~(#6je}G,`3ffoDMblo8*zY9{6DRu92gak%?_9RD g7r}VA`1' );
define( 'LOGGED_IN_KEY',    'OmA<.ErI1y_#(d@ @b_o7%#Mvx14-uYW{.@MwL4A=[J~.I-Vy~v:Mhfljh$H RMq' );
define( 'NONCE_KEY',        'dkT}gb{xs1S3E}&oy*NC,qnu+_L8_Z3N)~7yg?`<6x.bCNkKYSb)_l{.h)){[Xv8' );
define( 'AUTH_SALT',        ':},2:;IP<y6G[Gc(#}OPaLQcb^4K&VpA=zXnS8-TBTG1V|C3Gw1{0~nUTQpo<T}/' );
define( 'SECURE_AUTH_SALT', 'z0cgwurC!fr/7U<IYmW8bs~+jGcYsX02.}kFvE;h3]<g_2 4XM>vgRh E*b>^Pb.' );
define( 'LOGGED_IN_SALT',   'k1~j d=YOjx4+IuCHD3$Yl@ia :W8^JAaNv2BqcatZ-iqz6VO~KdG<Ii=5XqQ&FE' );
define( 'NONCE_SALT',       'P$xKf|^2j[3mQgX4?6na0TN>}rY$F6}BsE=.(=Js&z4<,4zMc`.i6!g%~Asn !zO' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
